describe('Sauce-demo-webLogin', () => {
    beforeEach(() => {
        cy.visit("/");
    });

    it('Prueba compra sauce-demo', () => {
        cy.fixture("login.json")
            .then(({ username, password }) => {
                cy.get('[data-test=username]').type(username);
                cy.get('[data-test=password]').type(password);
                cy.url().should("include", "/inventory.html");
        cy.get(':nth-child(1) > .pricebar > .btn_primary').click();
        cy.get('.svg-inline--fa').click();
        cy.url().should("include", "/cart.html");
        cy.get(".fa-layers-counter").contains("1");
        cy.get(".btn_action").click();
        cy.fixture("addressData1.json")
                .then(({ firstName, lastName, postalCode }) => {
                    cy.get('[data-test=firstName]').type(firstName);
                    cy.get('[data-test=lastName]').type(lastName);
                    cy.get('[data-test=postalCode]').type(postalCode);
                    cy.get('.btn_primary').click();
                });
        cy.url().should("include", "/checkout-step-two.html");
        cy.get('.btn_action').click();
        cy.url().should("include", "/checkout-complete.html");
            });
    });
    
    it('Prueba compra sauce-demo2', () => {
                cy.get('[data-test=username]').type("problem_user");
                cy.get('[data-test=password]').type("secret_sauce");
                cy.get("form").submit();
                cy.url().should("include", "/inventory.html");
        cy.get('#item_4_title_link').click();
        cy.get('.btn_primary').click();
        cy.get('.svg-inline--fa').click();
        cy.url().should("include", "/cart.html");
        cy.get(".fa-layers-counter").contains("1");
        cy.get(".btn_action").click();
        cy.fixture("addressData1.json")
                .then(({ firstName, lastName, postalCode }) => {
                    cy.get('[data-test=firstName]').type(firstName);
                    cy.get('[data-test=lastName]').type(lastName);
                    cy.get('[data-test=postalCode]').type(postalCode);
                    cy.get('.btn_primary').click();
                });
        cy.url().should("include", "/checkout-step-two.html");
        cy.get('.btn_action').click();
        cy.url().should("include", "/checkout-complete.html");
            }); 
    });